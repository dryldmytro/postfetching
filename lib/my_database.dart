import 'dart:convert';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:userposts/models/comment.dart';
import 'models/post.dart';
import 'package:http/http.dart' as http;
import 'models/comment.dart';

class MyDatabase {
  Database db;

  void createDB() async {
    db = await openDatabase(join(await getDatabasesPath(), "my_db.db"),
        onCreate: onCreate, version: 1);
    print(db);
  }

  Future<void> onCreate(Database db, int version) async {
    await createTables(db);
    await fetchPosts();
    await fetchComments();
  }

  Future<void> createTables(Database db) async {
    await db.execute(
        "CREATE TABLE IF NOT EXISTS posts(userId INTEGER, id INTEGER PRIMARY KEY,"
        "title TEXT, body TEXT)");
    await db.execute(
        "CREATE TABLE IF NOT EXISTS comments(postId INTEGER, id INTEGER PRIMARY KEY, name TEXT,"
        "email TEXT, body TEXT, FOREIGN KEY(postId) REFERENCES posts (id))");
  }

  Future<List<Post>> getAllPosts() async {
    final sql = "SELECT * FROM posts";
    final data = await db.rawQuery(sql);
    List<Post> posts = List();
    for (final node in data) {
      Post post = Post(
          userId: node["userId"],
          id: node["id"],
          title: node["title"],
          body: node["body"]);
      posts.add(post);
    }
    return posts;
  }

  Future<void> addPost(Post post) async {
    final sql =
        "INSERT INTO posts(userId,id,title,body) VALUES(${post.userId},${post.id},'${post.title}','${post.body}')";
    final result = await db.rawInsert(sql);
  }

  Future<void> addComment(Comment comment) async {
    final sql =
        "INSERT INTO comments(postId,id,name,email,body) VALUES(${comment.postId},${comment.id},'${comment.name}',"
        "'${comment.email}','${comment.body}')";
    final result = await db.rawInsert(sql);
  }

  Future<List<Comment>> getCommentsbyPostId(int postId) async {
    final sql = "SELECT * FROM comments WHERE postId=$postId";
    final data = await db.rawQuery(sql);
    List<Comment> comments = List();
    for (final node in data) {
      Comment comment = Comment(
          postId: node["postId"],
          id: node["id"],
          name: node["name"],
          email: node["email"],
          body: node["body"]);
      comments.add(comment);
    }
    return comments;
  }

  void fetchPosts() async {
    final response =
        await http.get('https://jsonplaceholder.typicode.com/posts');
    if (response.statusCode == 200) {
      var allData = json.decode(response.body) as List<dynamic>;
      allData.forEach((jsonPost) {
        Post post = Post(
            userId: jsonPost["userId"],
            id: jsonPost["id"],
            title: jsonPost["title"],
            body: jsonPost["body"]);
        addPost(post);
      });
    } else {
      print("Can't load data");
    }
  }

  void fetchComments() async {
    final response =
        await http.get("https://jsonplaceholder.typicode.com/comments");
    if (response.statusCode == 200) {
      var allData = json.decode(response.body) as List<dynamic>;
      allData.forEach((jsonComment) {
        Comment comment = Comment(
            postId: jsonComment["postId"],
            id: jsonComment["id"],
            name: jsonComment["name"],
            email: jsonComment["email"],
            body: jsonComment["body"]);
        addComment(comment);
      });
    } else
      print("Can't load data");
  }
}
