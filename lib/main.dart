import 'package:flutter/material.dart';
import 'package:userposts/pages.dart';
import 'package:userposts/my_database.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  MyDatabase myDatabase = MyDatabase();
  await myDatabase.createDB();
  runApp(MyApp(myDatabase));
}

class MyApp extends StatelessWidget {
  MyDatabase myDatabase;

  MyApp(this.myDatabase); // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MainPage(myDatabase));
  }
}
