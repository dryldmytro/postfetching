import 'package:flutter/material.dart';
import 'package:userposts/my_database.dart';
import 'package:userposts/models/comment.dart';

import 'models/post.dart';

class MainPage extends StatefulWidget {
  MyDatabase database;

  MainPage(this.database);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Posts"),
      ),
      body: Container(
        child: FutureBuilder<List<Post>>(
          future: widget.database.getAllPosts(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    final item = snapshot.data[index];
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  SecondScreen(item.id, widget.database),
                            ));
                        widget.database.getCommentsbyPostId(item.userId);
                      },
                      child: ListTile(
                        title: Text(
                            "userID: ${item.userId}, Title: ${item.title}"),
                        subtitle: Text(item.body),
                      ),
                    );
                  });
            } else if (snapshot.hasError) {
              return Text("some wrong");
            }
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}

class SecondScreen extends StatelessWidget {
  final int id;
  final MyDatabase database;

  SecondScreen(this.id, this.database);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Comments"),
      ),
      body: Container(
        child: FutureBuilder<List<Comment>>(
          future: database.getCommentsbyPostId(id),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    final item = snapshot.data[index];
                    return ListTile(
                      title: Text("email: ${item.email}, Name: ${item.name}"),
                      subtitle: Text(item.body),
                    );
                  });
            } else if (snapshot.hasError) {
              return Text("some wrong");
            }
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}
